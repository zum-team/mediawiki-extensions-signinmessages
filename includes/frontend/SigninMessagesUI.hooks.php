<?php

/**
 * Class containing hooked functions for a SigninMessages environment
 */
class SigninMessagesUIHooks {

	/**
	 * @param \MediaWiki\Auth\AuthenticationRequest[] $requests
	 * @param array $fieldInfo
	 * @param array &$formDescriptor
	 * @param string $action
	 * @return void
	 * @throws ErrorPageError
	 */
	public static function onAuthChangeFormFields(
		array $requests,
		array $fieldInfo,
		array &$formDescriptor,
		string $action
	) {
		$msgPostLoginprompt = wfMessage( 'signinmessages-post-loginprompt' );
		if ( $msgPostLoginprompt->exists() ) {
			$formDescriptor[] = [
				'type' => 'info',
				'label-message' => $msgPostLoginprompt->getKey(),
				'weight' => -300, // -205 is used for login page error area
			];
		}
		$msgPreLoginButton = wfMessage( 'signinmessages-pre-login-button' );
		if ( $msgPreLoginButton->exists() ) {
			$formDescriptor[] = [
				'type' => 'info',
				'label-message' => $msgPreLoginButton->getKey(),
				'weight' => 99,
			];
		}
		$msgPostLoginButton = wfMessage( 'signinmessages-post-login-button' );
		if ( $msgPostLoginButton->exists() ) {
			$formDescriptor[] = [
				'type' => 'info',
				'label-message' => $msgPostLoginButton->getKey(),
				'weight' => 100,
			];
		}
	}
}
